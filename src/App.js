import Navbar from "./component/Navbar";
import BodyContainer from "./component/BodyContainer";
import Footer from "./component/Footer/Footer";

function App() {
  return (
    <>
      <Navbar />
      <BodyContainer />
      <Footer />
    </>
  );
}

export default App;
