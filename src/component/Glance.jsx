import { BsFillPlayCircleFill } from "react-icons/bs";
function Glance() {
  return (
    <section className="glance-section">
      <div className="fontWeight-600 mb-4 mt-4">At a Glance</div>
      <div className="d-flex justify-content-between mb-4 gap-2">
        <div className="glance-sub-container-1 d-flex flex-column gap-3">
          <div className=" d-flex justify-content-between gap-5">
            <div className="glance-div ">
              <div className="glance-title mb-2">ETFs</div>
              <div>4</div>
            </div>
            <div className="glance-div">
              <div className="glance-title mb-2">Rebalance Frequency</div>
              <div>Quarterly</div>
            </div>
          </div>
          <div className="d-flex justify-content-between gap-5">
            <div className="glance-div">
              <div className="glance-title mb-2">Last Rebalance</div>
              <div>Jan 2, 2023</div>
            </div>
            <div className="glance-div">
              <div className="glance-title mb-2">Next Rebalance</div>
              <div>Apr 3, 2023</div>
            </div>
          </div>
        </div>
        <div className="glance-sub-container-2">
          <div className="mb-3">Check latest rebalance updates issued by the manager</div>
          <div className="color-blue">View Rebalance Timeline</div>
        </div>
      </div>
      <div className="border aboutSmallCase-hr mb-3"></div>
      <div className="d-flex mb-2">
        <div className="glance-understand">Understand how rebalancing works.</div>
        <div className="color-blue">
          Watch Video <BsFillPlayCircleFill />
        </div>
      </div>
      <div className="d-flex mb-5">
        <div className="glance-understand">
          Compare fundamental ratios of smallcase vs. Equity Large Cap.</div>
        <div className="color-blue">Check Financial Ratios</div>
      </div>
    </section>
  );
}

export default Glance;
