import { MdHomeFilled } from "react-icons/md";

function AboutUs() {
  return (
    <section>
      <div className="d-flex gap-2 mb-5 mt-4">
        <div>
          <MdHomeFilled className="aboutus-home" />
        </div>
        <div>&gt;</div>
        <div>smallcases</div>
        <div>&gt;</div>
        <div>All Weather Investing</div>
      </div>
      <div className="d-flex justify-content-between fontSize-small">
        <div className="aboutus-smallcase-description">
          <div className="mb-4">
            <img
              src="https://www.smallcase.com/static/svgs/smallcase-logo-with-text.svg"
              alt=""
            />
          </div>
          <div className="mb-3">
            smallcase Technologies builds platforms & investment products to
            invest better in Indian equities. A smallcase is a basket of
            stocks/ETFs curated to reflect an idea
          </div>
          <div className="mb-3">
            Investing in Stocks/ETFs (Exchange Traded Funds) are subject to
            market risks. Read all the related documents before investing.
            Investors should consider all risk factors and consult their
            financial advisor before investing
          </div>
        </div>
        <div className="d-flex flex-column gap-1">
          <div className="fontWeight-600">Company</div>
          <div>About</div>
          <div>For Businesses</div>
          <div>Blog</div>
          <div>Press</div>
          <div>Careers</div>
        </div>
        <div className="d-flex flex-column gap-1">
          <div className="fontWeight-600">Products</div>
          <div>Publisher</div>
          <div>Share</div>
          <div>Gateway</div>
          <div>Brokers</div>
          <div>Tickertape</div>
        </div>
        <div className="d-flex flex-column gap-1">
          <div className="fontWeight-600">Fine Print</div>
          <div>Privacy</div>
          <div>Disclosures</div>
          <div>User Created smallcases</div>
          <div>Investment Toold</div>
          <div>Return Calculation Methodology</div>
        </div>
      </div>
      <div className="aboutus-input-button">
        <div className="mb-2 fontSize-small fontWeight-600">
          Get weekly market insights and facts right in your inbox
        </div>
        <form className="mb-4">
          <input type="email" placeholder="Email Address" className="aboutus-input" />
          <button className="btn btn-primary  fontSize-small">
            Subscribe
          </button>
        </form>
      </div>
      <div className="border"></div>
    </section>
  );
}

export default AboutUs;
