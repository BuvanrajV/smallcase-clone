import {AiFillFacebook} from"react-icons/ai"
import { AiOutlineTwitter } from "react-icons/ai";
import {BsInstagram} from "react-icons/bs"
import {BsYoutube} from "react-icons/bs"
import {AiFillLinkedin} from "react-icons/ai"
import {CgProfile} from "react-icons/cg"


function ContactUs() {
  return (
    <div className="d-flex justify-content-between contactus-container fontSize-small mt-4 mb-4">
      <div>
        <div>smallcase Technologies Private Limited</div>
        <div>#51, 3rd Floor, Le Parc Richmonde,</div>
        <div>Richmond Road, Shanthala Nagar,</div>
        <div>Richmond Town, Bangalore - 560025</div>
      </div>
      <div>
        <div className="mb-2">Contact us on help@smallcase.com</div>
        <div className="mb-2">Find us on:</div>
        <div className="d-flex gap-1 " style={{marginLeft:"-0.5vw"}} >
          <div ><AiFillFacebook className="contactus-icon"/></div>
          <div><AiOutlineTwitter className="contactus-icon"/></div>
          <div><BsInstagram className="contactus-icon"/></div>
          <div><BsYoutube className="contactus-icon"/></div>
          <div><AiFillLinkedin className="contactus-icon"/></div>
        </div>
      </div>
      <div>
        <div className="mb-3">Help & Support</div>
        <div className="d-flex">
          <div><CgProfile className="contactus-profile-icon"/></div>
          <div>
            <div className="contactus-usedBy">USED BY</div>
            <div>70 lakh+ Users</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ContactUs;
