import AboutUs from "./AboutUs";
import ContactUs from "./ContactUs";

function Footer() {
  return (
      <footer className="footer container-box color-535 border-top">
        <AboutUs />
        <ContactUs />
      </footer>
  );
}

export default Footer;
