import {MdStars} from"react-icons/md"

function Investors(){
    return (
        <div className="investors-border mt-5">
            <div className="investors-trending bg-white color-444"><MdStars className="investors-icon color-444" size={"20"}/>Trending</div>
            <div className="color-535 mt-3 color-535">Watchlisted by over 10K investors</div>
        </div>
    )
}

export default Investors;