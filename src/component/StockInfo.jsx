import React, { Component } from 'react';
import { getDataFromApi } from './api';

class StockInfo extends Component {
    state = {
        isPopup : true,
      } 
    componentDidMount(){
        getDataFromApi().then((response)=>{
            console.log(response.data.data)
        })
    }
    render() { 
        return (
            <section className="d-flex justify-content-between stockInfo-section">
                
            <div className="d-flex gap-3">
                <div>
                    <img src="https://assets.smallcase.com/images/smallcases/160/SCAW_0001.png" alt="All Weather Investing Logo" className="stockInfo-logo"/>
                </div>
                <div className="d-flex flex-column gap-1">
                    <div className="d-flex gap-3">
                        <div><h4 className="fontWeight-600">All Weather Investing</h4></div>
                        <div className="stockInfo-free-access color-1F7 fontWeight-600" >Free access</div>
                    </div>
                    <div className="color-6C7 fontSize-small">Managed by Windmill Capital</div>
                    <div className='color-2F3'>One investment for all market conditions.Works for everyone</div>
                </div>
            </div>
            <div className="d-flex gap-5 align-items-center">
                <div className="d-flex flex-column mb stockInfo-cursor">
                    <div className=" stockInfo-cagr color-818">5Y CAGR</div>
                    <div className="stockInfo-percentage">10.24%</div>
                </div>
                <div className='stockInfo-volatility-div'><button className="stockInfo-volatility stockInfo-cursor bg-white fontSize-small color-535">Low Volatility</button></div>
            </div>
            {/* <div>
                
            </div> */}
        </section>
        );
    }
}
 
export default StockInfo;
