import axios from "axios";

export function getDataFromApi() {
  return axios.get(`/smallcases/smallcase?scid=SCAW_0001`);
}
