import { IoIosNotifications } from "react-icons/io";

function Navbar() {
  return (
    <header className="navbar fixed-top border-bottom container-box bg-white ">
      <nav className="d-flex navbar-navTag" >
        <div className="d-flex navbar-container color-818 mr-auto">
          <div className="navbar-cursor-pointer"><img src="https://www.smallcase.com/static/svgs/logo-full.svg" alt="smallcase" className="navbar-img-smallcase h-100 w-100" /></div>
          <div className="navbar-cursor-pointer">Home</div>
          <div className="navbar-cursor-pointer">Discover</div>
          <div className="navbar-cursor-pointer">Create</div>
        </div>
        <div className="d-flex navbar-container ml-auto mr-0">
          <div className="navbar-cursor-pointer">Watchlist</div>
          <div className="navbar-cursor-pointer">Investments</div>
          <div className="navbar-cursor-pointer"><IoIosNotifications size={25}/></div>
          <div className="navbar-cursor-pointer">Account</div>
        </div>
      </nav>
    </header>
  );
}

export default Navbar;
