import { BsFilePostFill } from "react-icons/bs";
import { MdScience } from "react-icons/md";
import { BsFileEarmarkSpreadsheetFill } from "react-icons/bs";

import { Link } from "react-router-dom";

function AboutSmallcase() {
  return (
    <section className="amoutSmallCase-section mt-5">
      <div className="d-flex gap-4">
        <div className="aboutSmallCase-overview">Overview</div>
        <Link to="/stocks">
        <div className="minimum-invest-cursor-pointer">ETFs & Weights</div>
        </Link>
      </div>
      <div className="border aboutSmallCase-hr"></div>
      <div className="d-flex gap-5 mt-4">
        <div className="aboutSmallCase-description">
          <div className="fontWeight-600">About the smallcase</div>
          <div className="mt-4">
            <div>
              All Weather Investing is a popular strategy that ensures your
              investments do well in good as well as bad times. This is a
              long-term investment strategy that you can use to build wealth
              over the years to come.
            </div>
            <div>
              <ul style={{fontSize:"0.9rem"}}>
                <li >
                  This smallcase invests in 3 asset classes--equity, debt and
                  gold
                </li>
                <li>
                  The portfolio is rebalanced periodically to generate
                  relatively higher returns by assuming the least possible risks
                </li>
              </ul>
            </div>
          </div>
          <div>
            This smallcase is ideal for all types of market conditions. It will
            ensure that neither will your investment ship sink, nor will the
            investment flight soar to scary heights. What you will get here is a
            steady ride to help you meet your long-term investment goals.
          </div>
        </div>
        <div className="d-flex flex-column gap-3 mt-4">
          <div className="d-flex gap-3">
            <div className="aboutSmallCase-icon-div">
              <BsFilePostFill className="color-248" />
            </div>
            <div>
              <div className="color-248">Blog Post</div>
              <div className="fontSize-small">
                Read more about All Weather Investing
              </div>
            </div>
          </div>
          <div className="d-flex gap-3">
            <div className="aboutSmallCase-icon-div">
              <MdScience className="color-248" />
            </div>
            <div>
              <div className="color-248">Methodology</div>
              <div className="fontSize-small">
                Know how this smallcase was created
              </div>
            </div>
          </div>
          <div className="d-flex gap-3">
            <div className="aboutSmallCase-icon-div">
              <BsFileEarmarkSpreadsheetFill className="color-248" />
            </div>
            <div>
              <div className="color-248">Factsheet</div>
              <div className="fontSize-small">
                Download key point of this smallcase
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
export default AboutSmallcase;
