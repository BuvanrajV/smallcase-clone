import { Link } from "react-router-dom";
import Glance from "./Glance";

function ETFsAndWeights() {
  return (
    <section>
      <div className="d-flex gap-4 mt-5">
        <Link to="/">
        <div className=" minimum-invest-cursor-pointer">Overview</div>
        </Link>
        <div className="Etfs-weights">ETFs & Weights</div>
      </div>
      <div className="border aboutSmallCase-hr mb-3"></div>
      <Glance />
    </section>
  );
}

export default ETFsAndWeights;
