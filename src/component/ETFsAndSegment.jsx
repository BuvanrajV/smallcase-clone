function ETFsAndSegment() {
  return (
    <section>
      <div className="fontWeight-600 mb-4">ETFs & Weights</div>
      <div className="d-flex">
        <div className="w-50">
          <div className="d-flex flex-column gap-2">
            <div className="d-flex justify-content-between color-535">
              <div>ETFs & Segments</div>
              <div>Weightage (%)</div>
            </div>
            <div className="d-flex justify-content-between color-535 border-top">
              <div className=""><span className="ETFsSegment-span" style={{background:"#FFC71F"}}></span>Equity</div>
              <div>41.68</div>
            </div>
            <div className="d-flex justify-content-between">
              <div>Nippon India ETF Nifty BeES</div>
              <div>14.71</div>
            </div>
            <div className="d-flex justify-content-between">
              <div className="color-1F7 fontWeight-600">
                Nippon India ETF Junior BeES
              </div>
              <div>26.97</div>
            </div>
            <div className="d-flex justify-content-between color-535 border-top">
              <div><span className="ETFsSegment-span" style={{background:"#14C2CE"}}></span>Debt</div>
              <div>24.39</div>
            </div>
            <div className="d-flex justify-content-between">
              <div className="color-1F7 fontWeight-600">
                Nippon India ETF Liquid BeES
              </div>
              <div>24.38</div>
            </div>
            <div className="d-flex justify-content- color-535 border-top ">
              <div><span className="ETFsSegment-span" style={{background:"#FC7A22"}}></span>Gold</div>
              <div>33.93</div>
            </div>
            <div className="d-flex justify-content-between">
              <div className="color-1F7 fontWeight-600">
                Nippon India ETF Gold BeES
              </div>
              <div>33.92</div>
            </div>
          </div>
        </div>
        <div className="w-50"></div>
      </div>
    </section>
  );
}

export default ETFsAndSegment;
