import { BrowserRouter, Route, Switch } from "react-router-dom";
import StockInfo from "./StockInfo";
import MinimumInvest from "./MinimumInvest";
import Investors from "./Investors";
import Overview from "./Overview";
import StockETFsAndWeights from "./StocksETFsAndWeights";

function BodyContainer() {
  return (
    <BrowserRouter>
      <div className="container-box">
        <StockInfo />
        <div className="d-flex gap-5">
          <div style={{width:"70%"}}>
            <Investors />
            <Switch>
              <Route exact path={"/"} component={Overview}/>
              <Route exact path={"/stocks"} component ={StockETFsAndWeights}/>
            </Switch>
          </div >
          <div style={{width:"30%"}}>
          <MinimumInvest />

          </div>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default BodyContainer;
