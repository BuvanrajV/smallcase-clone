import {TfiReload} from "react-icons/tfi"
import { IoLogoFacebook } from "react-icons/io";
import { AiOutlineTwitter } from "react-icons/ai";
import { IoLogoWhatsapp } from "react-icons/io";
import { ImEmbed } from "react-icons/im";
import { RiFileCopyFill } from "react-icons/ri";

function MinimumInvest() {
  return (
    <section className="minimumInvest-section mt-5 w-100">
      <div className=" color-818 mb-1">Minimum Investment Amount</div>
      <div className="minimumInvest-amount mb-3">₹ 4,185 <TfiReload className="minimumInvest-reload-icon"/></div>
      <div className="fontSize-small color-535">Get free access forever</div>
      <div className="color-1F7 fontSize-small mb-3 fontWeight-600 cursor-pointer">See more benefits</div>
      <div>
        <button className=" btn btn-success btn-lg minimumInvest-invest-button w-100 fontSize-small mb-4">Invest Now</button>
      </div>
      <div>
        <button className=" btn btn-outline-primary btn-lg minimumInvest-watchlist-button w-100 fontSize-small mb-4">Add to Watchlist</button>
      </div>
      <div className="border mb-4"></div>
      <div className="d-flex gap-3 color-818 fontSize-small">
        <div>Share on</div>
        <div className="minimum-invest-cursor-pointer">
          <IoLogoFacebook />
        </div>
        <div className="minimum-invest-cursor-pointer">
          <AiOutlineTwitter />
        </div>
        <div className="minimum-invest-cursor-pointer">
          <IoLogoWhatsapp />
        </div>
        <div className="minimum-invest-cursor-pointer">
          <ImEmbed />
        </div>
        <div className="minimum-invest-cursor-pointer">
          <RiFileCopyFill />
        </div>
      </div>
    </section>
  );
}

export default MinimumInvest;
