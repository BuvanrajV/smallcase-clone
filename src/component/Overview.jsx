import React, { Component } from "react";
import AboutSmallcase from "./AboutSmallcase";
import AboutTheManger from "./AboutTheManger";

class Overview extends Component {
  state = {};
  render() {
    return (
      <>
        <AboutSmallcase />
        <AboutTheManger />
      </>
    );
  }
}

export default Overview;
