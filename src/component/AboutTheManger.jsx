import {AiFillPlusCircle} from "react-icons/ai";
import {RiLoginCircleFill} from "react-icons/ri";
import {GiWaves} from "react-icons/gi"

function AboutTheManger(){
    return(
     <section className="manager-section mt-5">
        <h5 className="font-weight-bold" style={{fontWeight:"bold"}}>About the Manager</h5>
        <div className="d-flex manager-container">
            <div className="manager-sub-container w-52">
                <div className="manager-windmill">Windmill Capital <AiFillPlusCircle className="manager-plus-icon"/></div>
                <div className="text-muted mb-3 manager-manages ">Manages 56 smallcases</div>
                <div className="mb-3 manager-description">We’re SEBI registered research analyst creating Thematic & Quantamental curated stock/ETF portfolios. Data analysis is the heart and soul behind our portfolio construction & with 50+...</div>
                <div className="d-flex gap-2">
                    <div><button className="manager-button"><RiLoginCircleFill className="manager-icon-button"/> Fundamental</button> </div>
                    <div><button className="manager-button"><GiWaves className="manager-icon-button"/> Sector Tracker</button></div>
                    <div> <button className="manager-button">+13 more strategies</button></div>
                </div>
            </div>
            <div className="d-flex manager-sub-container w-45">
                <img src="https://assets.smallcase.com/images/publishers/smallcaseHQ/logo.png" alt="WINDMILL CAPITAL IMG" className="manager-img" />
            </div>
        </div>
     </section>
    )
}

export default(AboutTheManger)