import ETFsAndWeights from "./ETFsAndWeights";
import ETFsAndSegment from "./ETFsAndSegment";
function StockETFsAndWeightWeights(){

    return(
        <div>
            <ETFsAndWeights />
            <ETFsAndSegment />
        </div>
    )
}

export default StockETFsAndWeightWeights;